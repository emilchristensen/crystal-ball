//
//  AppDelegate.h
//  Crystal Ball
//
//  Created by Emil Rømer Christensen on 1/14/13.
//  Copyright (c) 2013 Vertic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
