//
//  ViewController.h
//  Crystal Ball
//
//  Created by Emil Rømer Christensen on 1/14/13.
//  Copyright (c) 2013 Vertic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
  NSArray *predictionArray;
}

@property (strong, nonatomic) IBOutlet UILabel *predictionLabel;
@property (strong, nonatomic) NSArray *predictionArray;
@property (strong, nonatomic) UIImageView *imageView;

- (void)makePrediction;

@end
